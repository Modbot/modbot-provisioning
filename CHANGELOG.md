# Modbot Provisioning
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [3.0.1] - 2018-10-23

### Changed

- References to Etherlab\_EtherCAT\_Master branch `v1.5.2-sncn-5\*` have been removed. The repository
ansible-ethercat defaults to `master` branch.

## [3.0.0] - 2018-07-31

### Added

- `ansible-modbot-development` requirement, a role which manages all modbot development resources.
- `setup.sh` prompts for packages to be installed.

### Changed

- `setup.sh` now includes methods to install apt-get, brew and brew cask packages.
- `setup.sh` has better text formatting.
- `setup.sh` takes different command arguments.
- `setup.sh` prompts for different installs (now RTPreempt, EtherCAT and Modbot Launcher).


## [2.0.0]

### Added

- `ansible-clone-repositories` requirement, a role which clones provided repositories.
- `ansible-modbot-repositories` requirement, a role which specifies Modbot development repositores.
- `setup.sh` fails when no MAC addresses are provided for the EtherCAT master.

### Changed

- `setup.sh` printouts.


## [1.2.0] - 2018-05-23

### Added

- `setup.sh local` will now parse any ethercat MAC addresses from `interfaces.rb`.

### Changed

- `ansible-ethercat` role updated to `1.1.0`.
- `ansible-modbot-brain` role updated to `1.2.0`.

## [1.1.0]

### Added

- `README-LOCAL.md`: info and instructions for local machine provisioning.
- `README-VM.md`: info and instructions for virtual machine provisioning.

### Changed

- `setup.sh` script
    - Script no longer takes `-b` or `-r` options.
    - Ansible version removed. Now installs default version supplied by apt-get.
    - Updated script usage and print functions.
- `playbook.yml` Ansible playbook
    - Prompt for RTPreempt patch added.
- `README.md` simplified.
- `ansible-modbot-brain` role updated to `1.1.0`

## [1.0.0]

### Added

`setup.sh` script:

- Commands:
    - `./setup.sh local` to provision the local machine
    - `./setup.sh vm` to create and provision a vagrant box
    - args:
        - `-r` install RT Preempt Kernel Patch
        - `-b` download and install Modbot Brain binary
- Installs:
    - Vagrant 2.0.2 (Optional, used for VM)
    - Ansible 2.4.3
    - The 4.8.0-rt10 RTPreempt Kernel Patch (Optional)
        - via Modbot [ansible-rtpreempt role](https://bitbucket.org/Modbot/ansible-rtpreempt).
    - EtherCAT v1.5.2-sncn-5
        - via Modbot [ansible-ethercat role](https://bitbucket.org/Modbot/ansible-ethercat).
    - Brain Dependencies
        - ROS Kinetic (jalessio's role)
        - ROS Kinetic URDF
        - ROS Kinetic Trac-IK
        - Go (JoshuaLund's role)
        - via Modbot [ansible-modbot-brain role](https://bitbucket.org/Modbot/ansible-modbot-brain).
    - Modbot Brain (Optional)
        - via Modbot [ansible-modbot-brain role](https://bitbucket.org/Modbot/ansible-modbot-brain).


`devtool-install.sh` script:

- Commands:
    - `.devtool-install.sh [TARGETS]`
    - args:
        - `--all` - install all targets
- Installs:
    - Chrome
    - Minicom
    - Sublime
    - Teamviewer
    - Terminator
    - vim
