# Modbot Deployment

## About

This document contains instructions necessary to provision a local system with the modbot-provisioning tool.


## Usage

- Run the script to provision your system.
    - Run`./setup.sh local`  
      The script will begin the installation process. It may prompt you for various optional installs, such as the RT Kernel patch, and development dependencies. 
    - If you choose to install the RT Kernel patch, Ansible will download, patch, build and install the RTPreempt Kernel. This process __**takes a long time**__ and __**requires an automatic reboot**__. Once the system has installed the kernel and rebooted, __**you will need to run `./setup local` once more**__ to complete the provisioning process.