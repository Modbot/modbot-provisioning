# Use this template file to create a file called `shared_directories.rb`.
@shared_directories = [
    {
        host_path: '/home/yourusername/sourcefolder/',
        guest_path: '/home/vagrant/destfolder/',
        owner: 'vagrant',
        group: 'vagrant'
    }
]