#!/bin/bash

#
# Modbot Provisioning Tool
#

# Formatting
NF='\033[0m'
BOLD='\033[1m'
LINE='\033[4m'
UBOLD='\033[21m'

TR='\033[31m'     # Red
TG='\033[32m'     # Green
TY='\033[33m'     # Yellow
TW='\033[97m'     # White

# Inputs
SELF=$0
TARGET=$1

# Ansible Extra Vars
ANSIBLE_EXTRA_VARS=""
ANSIBLE_ETHERCAT_MAC_DEFAULT="\"ff:ff:ff:ff:ff:ff\""
ANSIBLE_INSTALL_RTPREEMPT="UNDEFINED"
ANSIBLE_INSTALL_ETHERCAT="UNDEFINED"
ANSIBLE_INSTALL_ETHERCAT_FORCE="UNDEFINED"
ANSIBLE_INSTALL_MODBOT_LAUNCHER="UNDEFINED"
ANSIBLE_INSTALL_MODBOT_BRAIN_RUN_DEPS="UNDEFINED"
ANSIBLE_INSTALL_MODBOT_INTERNAL_DEV_DEPS="UNDEFINED"
ANSIBLE_USERNAME=$(whoami)


# Print Usage
usage () {
  echo -e "Usage : $SELF [OPTION] TARGET"
  echo -e "Provision your machine for the Modbot ecosystem."
  echo -e "$SELF local                                    provision local machine"
  echo -e "$SELF vm                                       provision virtual machine"
  echo -e " -l,   --modbot-launcher [YES / no]            install modbot launcher"
  echo -e " -r,   --rtpreempt [YES / no]                  install rtpreempt patch"
  echo -e " -u,   --username [USERNAME]                   username"
  echo -e " -d,   --internal-development [YES / no]       install internal development dependencies"
  exit
}

# Print Tool Information
print_info () {
  echo
  echo
  echo -e "   Modbot Provisioning"
  echo -e " =========================================================  "
  echo
  echo -e "   Welcome to the Modbot provisioning tool. This tool installs"
  echo -e "   software and libraries for the Modbot ecosystem."
  echo
  echo -e "   This tool installs:"
  echo -e "    - The Modbot Launcher"
  echo -e "    - The Modbot Brain runtime dependencies"
  echo -e "      - RTPreempt Kernel Patch"
  echo -e "      - EtherCAT Kernel Driver"
  echo -e "      - Other libraries (see README.md)"
  echo
  echo -e "  If you experience any issues, please contact the Modbot team."
  echo -e "  hello@modbot.com"
  echo
  echo -e " =========================================================  "
  echo
  echo
}

# Install Apt Package
install_apt_pkg() {
  local input_=$1
  if ! [ -x "$(command -v $input_ \>/dev/null)" ]; then
    echo -e "* Installing $input_."
    sudo apt-get install -y -qq $input_
  fi
}

# Install Brew Package
install_brew_pkg() {
  local input_=$1
  if ! [ -x "$(command -v $input_ \>/dev/null)" ]; then
    echo -e "* Installing $input_."
    brew install $input_
  fi
}

# Install Brew Cask
install_brew_cask_pkg() {
  local input_=$1
  if ! [ -x "$(command -v $input_ \>/dev/null)" ]; then
    echo -e "* Installing $input_."
    brew cask install $input_
  fi
}

# Install Ansible in Ubuntu
install_ansible_ubuntu() {
  if ! [ -x "$(command -v ansible \>/dev/null)" ]; then
    echo -e "* Installing Ansible."
    sudo apt-add-repository -y -u ppa:ansible/ansible
    sudo apt-get -qq update
    install_apt_pkg software-properties-common
    install_apt_pkg ansible
  fi
}

# Install v 2.0.2 of Vagrant in Ubuntu
install_vagrant_ubuntu () {
  if ! [ -x "$(command -v vagrant \>/dev/null)" ]; then
    echo -e "* Installing Vagrant v 2.0.2."
    wget -q https://releases.hashicorp.com/vagrant/2.0.2/vagrant_2.0.2_x86_64.deb?_ga=2.93379741.1055664532.1519152976-661074423.1518545868 -O mb-vagrant
    sudo dpkg -i mb-vagrant
    sudo apt-get -qq install -f
    rm mb-vagrant
  fi
}

# Parse Ansible Extra Vars
parse_ansible_extra_vars() {
  if [[ "$ANSIBLE_INSTALL_RTPREEMPT" == "UNDEFINED" ]]; then
    echo -e "* Install the 4.8.0-rt10 RTPreempt Kernel Patch? [Y/n] "
    read parse_input
    ANSIBLE_INSTALL_RTPREEMPT=$(parse_Yn $parse_input)
  fi

  if [[ "$ANSIBLE_INSTALL_ETHERCAT" == "UNDEFINED" ]]; then
    echo -e "* Install the EtherCAT Kernel Module? [Y/n] "
    read parse_input
    ANSIBLE_INSTALL_ETHERCAT=$(parse_Yn $parse_input)
  fi

  if [[ "$ANSIBLE_INSTALL_MODBOT_LAUNCHER" == "UNDEFINED" ]]; then
    echo -e "* Install the Modbot Launcher? [Y/n] "
    read parse_input
    ANSIBLE_INSTALL_MODBOT_LAUNCHER=$(parse_Yn $parse_input)
  fi

  if [[ "$ANSIBLE_INSTALL_MODBOT_BRAIN_RUN_DEPS" == "UNDEFINED" ]]; then
    echo -e "* Install the Modbot Brain Runtime Dependencies? [Y/n] "
    read parse_input
    ANSIBLE_INSTALL_MODBOT_BRAIN_RUN_DEPS=$(parse_Yn $parse_input)
  fi

  if [[ "$ANSIBLE_INSTALL_MODBOT_INTERNAL_DEV_DEPS" == "UNDEFINED" ]]; then
    echo -e "* Install the internal Modbot Development Resources? [y/N] "
    read parse_input
    ANSIBLE_INSTALL_MODBOT_INTERNAL_DEV_DEPS=$(parse_yN $parse_input)
  fi

  if [[ "$(parse_yN $ANSIBLE_INSTALL_ETHERCAT)" == "True" ]]; then
    if [[ "$ANSIBLE_INSTALL_ETHERCAT_FORCE" == "UNDEFINED" ]]; then
      echo -e "* Force re-install of the EtherCAT Kernel Module? [y/N] "
      read parse_input
      ANSIBLE_INSTALL_ETHERCAT_FORCE=$(parse_yN $parse_input)
    fi
    parse_ethercat
  fi
  
  ANSIBLE_EXTRA_VARS+="\"username\":\"$ANSIBLE_USERNAME\""
  ANSIBLE_EXTRA_VARS+=",\"ethercat_force_install\":\"$ANSIBLE_INSTALL_ETHERCAT_FORCE\""
  ANSIBLE_EXTRA_VARS+=",\"install_rtpreempt\":\"$ANSIBLE_INSTALL_RTPREEMPT\""
  ANSIBLE_EXTRA_VARS+=",\"install_ethercat\":\"$ANSIBLE_INSTALL_ETHERCAT\""
  ANSIBLE_EXTRA_VARS+=",\"install_modbot_launcher\":\"$ANSIBLE_INSTALL_MODBOT_LAUNCHER\""
  ANSIBLE_EXTRA_VARS+=",\"install_modbot_brain_run_deps\":\"$ANSIBLE_INSTALL_MODBOT_BRAIN_RUN_DEPS\""
  ANSIBLE_EXTRA_VARS+=",\"install_internal_dev_deps\":\"$ANSIBLE_INSTALL_MODBOT_INTERNAL_DEV_DEPS\""
  ANSIBLE_EXTRA_VARS+=",\"ethercat_mac_addresses\":[$ANSIBLE_ETHERCAT_MAC]"
}

# Install Cowsay in Ubuntu
install_cowsay_ubuntu () {
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    install_apt_pkg cowsay

  elif [[ "$OSTYPE" == "darwin"* ]]; then
    if [[ -z `which brew` ]]; then
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
    install_brew_pkg cowsay
  fi
}

# Parse Ethercat Mac Addresses
#   Parses MAC Addresses from interfaces.rb; formats them for Ansible --extra-vars
parse_ethercat() {
  ANSIBLE_ETHERCAT_MAC=""
  macs=($(grep -oP "(?<=mac: )[^,]+" interfaces.rb))
  n_interfaces=${#macs[*]}

  if [ "$n_interfaces" -ge "1" ]; then
    echo -e "* Found ${BOLD}${#macs[*]}${NF} EtherCAT master MAC addresses."
    for (( i=0; i<${#macs[*]}; ++i ))
    do
        addr_string=$(echo ${macs[$i]} | sed "s/'/\"/g")
        echo "*   $i - ${addr_string}"
        ANSIBLE_ETHERCAT_MAC+=${addr_string}
        if [[ "$i" -lt "((${#macs[@]}-1))" ]]; then
          ANSIBLE_ETHERCAT_MAC+=","
        fi
    done
    echo
    
  else
    ANSIBLE_ETHERCAT_MAC=$ANSIBLE_ETHERCAT_MAC_DEFAULT
    echo -e "* ${TY}Found ${#macs[*]} EtherCAT master MAC addresses.${NF}"
    echo -e "* ${TY}Using $ANSIBLE_ETHERCAT_MAC.${NF}"
  fi
}

parse_update() {
  echo -e "* Upgrade system with Apt-Get? [Y/n] "
  read parse_input
  if [[ "$(parse_yN $parse_input)" == "True" ]]; then
    sudo apt-get -y upgrade
  fi
}

# Parses an input string;
#   Echoes `True` if input is `Yes`, `ses`, `Y`, `y`, `True` or `true` or <enter>.
#   Echoes `False` otherwise.
parse_Yn() {
  local input_=$1
  if [[ "${input_,,}" == "y" ]]; then
    echo "True"
  elif [[ "${input_,,}" == "yes" ]]; then
    echo "True"
  elif [[ "${input_,,}" == "true" ]]; then
    echo "True"
  elif [[ "${input_,,}" == "" ]]; then
    echo "True"
  else
    echo "False"
  fi
}

# Parses an input string;
#   Echoes `True` if input is `Yes`, `yes`, `Y`, `y`, `True` or `true`.
#   Echoes `False` if <enter> or otherwise.
parse_yN() {
  local input_=$1
  if [[ "${input_,,}" == "" ]]; then
    echo "False"
  else
    echo $(parse_Yn $input_)
  fi
}


########################      APPLICATION      ########################

# Exit if root
if [ "$ANSIBLE_USERNAME" == "root" ]; then
  usage
  exit
fi

# Parse Arguments
while [ "$1" != "" ]; do
  case $1 in
    -a | --modbot-api)
      shift
      ANSIBLE_INSTALL_MODBOT_API=$(parse_Yn $1)
      ;;
    -b | --brain-runtime-deps)
      shift
      ANSIBLE_INSTALL_MODBOT_BRAIN_RUN_DEPS=$(parse_Yn $1)
      ;;
    -d | --internal-development)
      shift
      ANSIBLE_INSTALL_MODBOT_INTERNAL_DEV_DEPS=$(parse_Yn $1)
      ;;
    -e | --ethercat)
      shift
      ANSIBLE_INSTALL_ETHERCAT=$(parse_Yn $1)
      ;;
    -l | --modbot-launcher)
      shift
      ANSIBLE_INSTALL_MODBOT_LAUNCHER=$(parse_Yn $1)
      ;;
    -r | --rtpreempt)
      shift
      ANSIBLE_INSTALL_RTPREEMPT=$(parse_Yn $1)
      ;;
    -u | --username)
      shift
      ANSIBLE_USERNAME=$1
      ;;
    --cowsay )
      install_cowsay_ubuntu
      ;;
    --help )
      usage
      ;;
    esac
  shift
done

# Exit if invalid target
if [ "$TARGET" == "local" ] || [ "$TARGET" == "vm" ]; then
  print_info
  echo -e "* Provisioning for target '${BOLD}${TARGET}${NF}'."
else
  usage
  exit
fi


# Provision -- Local Machine
if [[ "$TARGET" == "local" ]]; then
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    # Install Ansible
    echo -e "* Updating Apt-Get."
    sudo apt-get -y -qq update
    install_apt_pkg git
    install_ansible_ubuntu
    parse_update
    parse_ansible_extra_vars

    # Provision with Ansible
    echo -e "* Provisioning with Ansible."
    echo
    echo -e " =========================================================  "
    echo

    ansible-galaxy install --roles-path provisioning/roles -r provisioning/requirements.yml --force
    ansible-playbook provisioning/playbook.yml \
      --ask-become-pass \
      --inventory provisioning/inventory \
      --extra-vars "{$ANSIBLE_EXTRA_VARS}"
    exit

  else
    echo -e "${TR}The Modbot Brain requires EtherCAT, currently only available on Linux.${NF}"
    echo -e "${TR}Please switch to a Linux OS, or provision a virtual machine using the argument ${BOLD}\`vm\`${NF}${TR}.${NF}"
    exit
  fi

# Provision -- Virtual Machine via Vagrant
#   Install VirtualBox, Vagrant, Ansible
#   Begin Vagrant VM + Ansible Provisioning
elif [[ "$TARGET" == "vm" ]]; then
  echo -e "* Installing dependencies required to create and provision a virtual machine."

  # Install VirtualBox, Vagrant, Ansible
  # Target Linux
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    install_apt_pkg virtualbox
    install_vagrant_ubuntu
    install_ansible_ubuntu
    install_apt_pkg git

  # Target MacOS
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    if [[ -z `which brew` ]]; then
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi

    install_brew_cask_pkg virtualbox
    install_brew_cask_pkg vagrant
    install_brew_pkg python
    install_brew_pkg ansible
  fi

  # Provision with Vagrant and Ansible
  echo -e "* ${TG}All requirements for a vagrant virtual machine have been met.${NF}"
  echo -e "* ${TY}Please configure your ${NF}${BOLD}interfaces.rb${NF}${TY} and optionally ${NF}${BOLD}shared_directories.rb${NF}${TY}.${NF}"
  echo -e "* ${TG}You may use the provided template files (rename them accordingly).${NF}"
  echo -e "* ${TG}When ready, hit ${NF}${BOLD}enter${NF}${TG} to proceed.${NF}"
  read  -n 0 dummy
  echo -e "* Running vagrant up --provision to provision the VM."
  echo

  vagrant up --provision
  exit

else
  exit
fi