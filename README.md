# Modbot Deployment

This repo contains scripts to install the requirements of the Modbot Brain and tools on a Ubuntu system or Vagrant Ubuntu Virtual Machine.


## About

This repo provides allows you to:  
- Provision a local machine (Ubuntu 16.04), or,  
- Bring up an Ubuntu virtual machine (Ubuntu, Mac OSX).


### Authors

- Antonia Elsen - antonia@modbot.com
- Simon Blee - simon@modbot.com


### Requirements

- If you are provisioning a vm with this tool, this tool requires: 
    - Ubuntu 16.04 or Mac OSX.
- If you are provisioning LOCALLY, this tool requires:
    -  Ubuntu 16.04, Kernels v4.2.x - v4.12.x
    - If you're not running on Ubuntu 16.04 in a compatible kernel, please use our Vagrant VM or spin up your own Ubuntu 16.04 VM within the appropriate kernel range. Kernel versions outside those listed may not be compatible with our EtherCAT library.


### Dependencies Installed

- Python                (if target VM, for OSX)
- Virtualbox            (if target VM)
- Vagrant               (if target VM)
- Ansible >= 2.5.0

Via Ansible:

- The 4.8.0-rt10 RTPreempt Kernel Patch (Optional)
    - via Modbot [ansible-rtpreempt role](https://bitbucket.org/Modbot/ansible-rtpreempt).
- Etherlab\_EtherCAT\_Master master branch
    - via Modbot [ansible-ethercat role](https://bitbucket.org/Modbot/ansible-ethercat).
- Brain Runtime Dependencies
    - Go
    - ROS Kinetic
    - ROS Kinetic URDF
    - ROS Kinetic Trac-Ik


## Usage

1. Clone this repo with Git
   - Run `sudo apt-get install git`
   - `cd` into the directory in which you'd like to clone this repo.
   - Run `git clone https://bitbucket.org/Modbot/modbot-provisioning.git`
2. Configure your EtherCAT master interfaces.
Create a file named `interfaces.rb` in the root of this project and provide the required information on the network interface(s) you wish to use for your EtherCAT master(s). See the example below, or use the `template_interfaces.rb` file, removing `template_` from the name.
    - Change the MAC address and bridge name in the example below to those of the network interface you wish to use for your EtherCAT master. To obtain this information, use;
        - `ifconfig` (Ubuntu)
            - The MAC address corresponds to the network's `HWaddr`
            - The bridge name corresponds to the name of the network interface
```ruby
@interfaces = [
    {
        ip:  '192.168.1.1',
        mac: 'ff:ff:ff:ff:ff:ff',
        bridge: 'enp25s0: Ethernet'
    }
]
```
3. (Optional) If you would like the provisioning tools to clone the Modbot repos for you, add your ssh key to bitbucket. This allows you to access the private Modbot repositories. If you do not have access to the private Modbot repositories, skip this step.
    - To check if you have an existing ssh key on your machine, run `ls ~/.ssh`. Public ssh keys will be of the `.pub` format.
    - To generate an ssh key if you do not already have one, run `ssh-keygen`. You may hit `<enter>` for any prompts.
    - To copy an ssh key, run `cat ~/.ssh/<ssh key name>.pub`. Most likely you will run `cat ~/.ssh/id_rsa.pub` unless you have specifically named your key something else. Copy the result. Or, open the file `~/.ssh/<ssh key name>.pub` in a text editor and copy the contents.
    - To add your ssh key to bitbucket, log into your account on bitbucket.org, and go to "Bitbucket Settings". There, you will find an "SSH Keys" section. Add the key here.
4. Run the script.
    - If you want to provision your local machine, please follow the instructions in `README-LOCAL.md`.
    - If you want to provision your virtual machine, please follow the instructions in `README-VM.md`.
