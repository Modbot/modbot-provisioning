#!/bin/bash
SELF=$0
TARGET=$1

# Install Developer Tools
  # Supports `all`` or a combination of any: chrome, minicom, sublime, terminator, teamviewer, vim, vscode, and xtime.

usage () {
  echo -e "Usage : $SELF TARGETS"
  echo -e "Install developer tools."
  echo -e "Targets:"
  echo -e " --all"
  echo -e "or;"
  echo -e " - chrome"
  echo -e " - minicom"
  echo -e " - sublime"
  echo -e " - terminator"
  echo -e " - teamviewer"
  echo -e " - vim"
  echo -e " - vscode"
  echo -e " - xtime [unimplemented]"
}

if [[ "$OSTYPE" != "linux-gnu" ]]; then
  usage
  echo
  echo -e "\033[0;31mDev-tool installation only supported on linux systems.\033[0m :("
  exit
fi

if [ "$#" -lt 1 ]; then
 usage
elif [ "$1" != "--help" ]; then
  # echo -e "* Installing dev tools."
  echo -e "  * Updating apt cache."
  sudo apt-get -qq update
fi

if [[ "$OSTYPE" != "linux-gnu" ]]; then
usage
echo
echo -e "\033[0;31mDev-tool installation only supported on linux systems.\033[0m :("
exit
fi

# Chrome
install_chrome () {
  echo -e "  * Installing chrome."
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
  sudo apt-get -qq -q update 
  sudo apt-get -qq -y install google-chrome-stable
}

# Minicom
install_minicom () {
  echo -e "  * Installing minicom."
  sudo apt-get -qq -y install minicom
}

# Sublime
install_sublime () {
  echo -e "  * Installing sublime."
  sudo add-apt-repository -y ppa:webupd8team/sublime-text-2
  sudo apt-get -q update
  sudo apt-get -qq -y install sublime-text
}

# Terminator
install_terminator () {
  echo -e "  * Installing terminator."
  sudo apt-get -qq -y install terminator 
}

# Teamviewer
install_teamviewer () {
  echo -e "  * Installing teamviewer."
  wget https://download.teamviewer.com/download/teamviewer_i386.deb
  sudo dpkg -i teamviewer_i386.deb
  sudo apt-get -qq -y install -f
  rm teamviewer_i386.deb
}

# Vim
install_vim () {
  echo -e "  * Installing vim."
  sudo apt-get -qq -y install vim
}

# VSCode
install_vscode () {
  echo -e "  * Installing vscode."
  sudo apt-get -qq -y install curl
  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
  sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
  sudo apt-get -qq update
  sudo apt-get -qq -y install code
}

# XTime
install_xtime () {
  echo -e "  * XTime installation not supported. :("
}


# Parse arguments
while [ "$1" != "" ]; do
  case $1 in
    --all )
      install_chrome
      install_minicom
      install_sublime
      install_teamviewer
      install_terminator
      install_vim
      install_vscode
      ;;
    chrome )
      install_chrome
      ;;
    minicom )
      install_minicom
      ;;
    sublime )
      install_sublime
      ;;
    teamviewer )
      install_teamviewer
      ;;
    terminator )
      install_terminator
      ;;
    vim )
      install_vim
      ;;
    vscode )
      install_vscode
      ;;
    --help )
      usage
      ;;
    esac
  shift
done