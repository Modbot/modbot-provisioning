# Modbot Deployment

## About

This document contains instructions necessary to create and provision a virtual machine with the modbot-provisioning tool.


## Usage

0. (Optional) If desired, configure the folders you would like to share between host and guest OSes. Create a file named `shared_directories.rb` in the root of this project and provide source and destination filepaths for your shared folders. See the example below, or use the `template_shared_directories.rb` file, removing `template_` from the name.
```ruby
@shared_directories = [
    {
        host_path: '/home/yourusername/sourcefolder/',
        guest_path: '/home/vagrant/destfolder/',
        owner: 'vagrant',
        group: 'vagrant'
    }
]
```

1. Run the script to provision your system.
    * `./setup.sh vm`. This script will begin installing the dependencies necessary to create the Vagrant virtual machine, and then provision it. 
    * If you choose to install the RT Kernel patch, Ansible will download, patch, build and install the RTPreempt Kernel. This process __**takes a long time**__ and __**requires a  reboot (with prompt)**__. Once the system has installed the kernel and rebooted, __**you will need to run `./setup local` once more**__ to complete the provisioning process.

### Running Vagrant Box

0. If the virtual machine is not already running, run `vagrant up`.
1. Run `vagrant ssh` to ssh into the VM.
2. Additional vagrant commands:
    - `vagrant halt` to shut down the VM.
    - `vagrant up --provision` to reprovision the VM.
    - `vagrant destroy` to destroy the VM.