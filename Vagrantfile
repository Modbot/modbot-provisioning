# Helper function to pluck item from an array
class Array
    def pluck(key)
        map {|h| h[key]}
    end
end

# Pull in the list of interfaces for this host
if File.file?('interfaces.rb')
  require_relative 'interfaces'
end
if ! defined? @interfaces
    @interfaces = []
end

# Pull in the list of shared directories
if File.file?('shared_directories.rb')
  require_relative 'shared_directories'
end
if ! defined? @shared_directories
    @shared_directories = []
end

dir = File.dirname(__FILE__)

# Now do Vagrant configuration
Vagrant.configure("2") do |config|
    config.vm.box = "bento/ubuntu-16.04"

    # Share ssh keys for using git on guest
    config.vm.synced_folder "#{Dir.home}/.ssh/", "/home/vagrant/.ssh/"
    config.vm.synced_folder "#{Dir.home}/.ssh/", "/root/.ssh/", owner: "root", group: "root"
    system("ssh-keygen -y -f #{dir}/.vagrant/machines/default/virtualbox/private_key >> ~/.ssh/authorized_keys")

    # Share all specified folders
    @shared_directories.each do |shared|
        config.vm.synced_folder shared[:host_path], shared[:guest_path], owner: shared[:owner], group: shared[:group]
    end

    # Allow external access to this guest
    config.vm.network "public_network"

    # Create public interface for Ethercat
    @interfaces.each do |iface|
        config.vm.network "public_network",
            bridge: iface[:bridge],
            mac: iface[:mac].tr(':', ''),
            ip: iface[:ip],
            :netmask => "255.255.255.0"
    end

    config.vm.provider "virtualbox" do |vb|
        vb.name = "modbot"
        vb.memory = 4096
        vb.cpus = 4
    end

    config.ssh.forward_agent = true

    # Configure the system
    config.vm.provision "ansible_local" do |ansible|
        mac_addresses = @interfaces.pluck(:mac)

        ansible.playbook = "provisioning/playbook.yml"
        ansible.galaxy_role_file = "provisioning/requirements.yml"
        ansible.extra_vars = {
            brain_directory: "/home/vagrant/brain/",
            ethercat_mac_addresses: mac_addresses.size ? mac_addresses : nil,
            ethercat_source_directory: "/home/vagrant/ethercat/",
            install_rtpreempt: "False",
            # install_internal_dev_deps: "False", # Set to true to install internal dev resources.
            username: "vagrant",
            vm: true,
        }
    end
end
