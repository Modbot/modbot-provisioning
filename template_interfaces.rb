# Use this template file to create a file called `interfaces.rb`.
@interfaces = [
    {
        ip: '192.168.1.1',
        mac: 'ff:ff:ff:ff:ff:ff',
        bridge: 'enp25s0: Ethernet'
    }
]